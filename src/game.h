#ifndef GAME_H
#define GAME_H

#include "map.h"
#include "character.h"
#include "donut.h"

// --------------- //
// Data structures //
// --------------- //

enum GameState {
	GAME_PLAY,	// The player is playing
	GAME_MENU,	// The player is returning to the menu
	GAME_WIN,	// The player won and is heading to the victory message
	GAME_LOSE,	// The player lost and is heading to the defeat message
	GAME_QUIT	// The player is quitting
};

struct Game {
	struct Map *map;			// The current map
	struct Character *character;// The character
	struct Donut *donut_1;		// The first donut
	struct Donut *donut_2;		// The second donut
	struct Donut *donut_3;		// The third donut
	int donutLeft;				// Number of donut find to satisfy win condition
	SDL_Renderer *renderer;		// The renderer
	enum GameState state;		// The state of the game
	int gameStartTime;			// The game's starting time
};

// --------- //
// Functions //
// --------- //

/**
 * Creates a new game.
 *
 * @param	The renderer for the game
 * @return	A pointer to the game, NULL if there was an error
 */
struct Game *Game_initialize(SDL_Renderer *renderer);

/**
 * Delete the game.
 *
 * @param game	The game to delete
 */
void Game_delete(struct Game *game);

/**
 * Start running the game.
 *
 * @param game	The game to run
 */
void Game_run(struct Game *game);

/**
 * Prevents the character from leaving the screen
 * by checking collision with the edges.
 *
 * @param game	The game that is currently running
 */
void Game_checkEdgeCollision(struct Game *game);

/**
 * Prevents the character from going through walls
 * by checking collision with the edges of the platforms.
 *
 * @param game	The game that is currently running
 */
void Game_checkWallCollision(struct Game *game);

/**
 * Display time left for the current game
 *
 * @param game	The game currently running
 */
void Game_RenderTimer(struct Game *game);
#endif
